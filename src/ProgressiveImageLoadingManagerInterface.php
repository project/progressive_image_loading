<?php

namespace Drupal\progressive_image_loading;

/**
 * Provides an interface for Progressive Image Loading manager.
 */
interface ProgressiveImageLoadingManagerInterface {

  /**
   * Returns the generated placeholder URL.
   *
   * @param string $url
   *   The source url.
   * @param int $width
   *   The width of the placeholder, in pixels.
   * @param int $height
   *   The height of the placeholder, in pixels.
   *
   * @return string
   *   The generated placeholder URL.
   */
  public function createPlaceholder(string $url, $width = 1, $height = 1);

  /**
   * Checks if the current path is excluded.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function currentPathIsExcluded();

}
