<?php

namespace Drupal\progressive_image_loading\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\progressive_image_loading\ProgressiveImageLoadingManagerInterface;

/**
 * Provides a background image base formatter.
 */
trait BackgroundImageFormatterBaseTrait {

  /**
   * Item classes to add.
   *
   * @var array
   */
  protected $itemClasses = [
    'progressive-image-loading',
    'progressive-image-loading-background',
  ];

  /**
   * The progressive image loading manager.
   *
   * @var \Drupal\progressive_image_loading\ProgressiveImageLoadingManagerInterface
   */
  protected $progressiveImageLoadingManager;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    unset($settings['image_link']);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    unset($elements['image_link']);

    return $elements;
  }

  /**
   * Sets the progressive image loading manager.
   *
   * @param \Drupal\progressive_image_loading\ProgressiveImageLoadingManagerInterface $progressive_image_loading_manager
   *   The progressive image loading manager.
   *
   * @return $this
   */
  protected function setProgressiveImageLoadingManager(ProgressiveImageLoadingManagerInterface $progressive_image_loading_manager) {
    $this->progressiveImageLoadingManager = $progressive_image_loading_manager;

    return $this;
  }

}
