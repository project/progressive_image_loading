<?php

namespace Drupal\progressive_image_loading\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of the 'Background Image' formatter.
 *
 * @FieldFormatter(
 *   id = "progressive_image_loading_background_image",
 *   label = @Translation("Background Image (Progressive Image Loading)"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class BackgroundImageFormatter extends ImageFormatter {


  use BackgroundImageFormatterBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->setProgressiveImageLoadingManager($container->get('progressive_image_loading.manager'));

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as $delta => $element) {
      foreach ($this->itemClasses as $class) {
        $elements[$delta]['#item_attributes']['class'][] = $class;
      }

      // Build tye background data attributes, as our lazy loader expects.
      $image_style = $this->imageStyleStorage->load($elements[$delta]['#image_style']);
      if ($image_style) {
        $original_uri = $elements[$delta]['#item']->entity->getFileUri();
        $derivative_uri = $image_style->buildUri($original_uri);
        // Make sure image style exists.
        if (!file_exists($derivative_uri)) {
          $image_style->createDerivative($original_uri, $derivative_uri);
        }

        $url = file_url_transform_relative($image_style->buildUrl($original_uri));
        $elements[$delta]['#item_attributes']['data-background-image'] = $url;
        // Adds a low quality image as default image.
        $elements[$delta]['#item_attributes']['style'] = 'background-image: url(' . $this->progressiveImageLoadingManager->createPlaceholder($url) . ');';
      }
    }

    return $elements;
  }

}
