/**
 * @file
 * Progressive Image Loading javascript.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Initialize Progressive Image Loading library.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.progressiveImageLoading = {
    attach: function (context, settings) {
      var overriddenSettings = {};

      if (typeof (settings.progressiveImageLoading) !== 'undefined') {
        overriddenSettings = settings.progressiveImageLoading;
      }

      new ProgressiveImageLoading(context, overriddenSettings);
    }
  };

})(jQuery, Drupal);
